require 'rubygems'
require 'ruby-debug'
require 'test/unit'
require 'net/http'
require File.dirname(__FILE__) + '/couchdb'
Debugger.start

class DatabaseTest < Test::Unit::TestCase
  include CouchDB
  def setup
    connection = Net::HTTP.new('localhost', '5984')
    connection.request Net::HTTP::Put.new('/test1')
    connection.request Net::HTTP::Delete.new('/test2')
  end
  
  def test_exists?
    assert Database.new('test1').exists?
    assert !Database.new('test2').exists?
  end
  
  def test_create
    db = Database.new('test2')
    db.create!
    assert db.exists?
  end
end

class DocumentTest < Test::Unit::TestCase
  include CouchDB
  def setup
    @db = Database.new('test1')
    connection = Net::HTTP.new('localhost', '5984')
    connection.request Net::HTTP::Delete.new('/test1/something')
  end
  
  def test_save
    doc = Document.new('hello' => 'world')
    assert @db.save(doc)
    assert doc.id
    assert doc.revision
    assert doc.database 
    doc.reload   

    assert_equal 1, doc.attributes.size
    doc['something'] = 'else'
    doc['hello'] = 'blah!'
    assert doc.save
    assert doc.revision
    doc.reload
    assert_equal 'else', doc['something']
    assert_equal 'blah!', doc['hello']
    assert_equal 2, doc.attributes.size
  end
  
  def test_bulk_update
    docs = [
      Document.new('a' => 1),
      Document.new('b' => 2),
      Document.new('c' => 3)
    ]
    
    assert @db.save(docs)
    
    docs.each { |doc| doc.reload }
    
    assert_equal 1, docs[0]['a']
    assert_equal 2, docs[1]['b']
    assert_equal 3, docs[2]['c']
    
    docs[0]['a'] = 11
    docs[1]['b'] = 22
    docs[2]['c'] = 33
    
    assert @db.save(docs)
    
    docs.each { |doc| doc.reload }
    assert_equal 11, docs[0]['a']
    assert_equal 22, docs[1]['b']
    assert_equal 33, docs[2]['c']
    
  end
end

class ViewTest < Test::Unit::TestCase
  include CouchDB
  def setup
    connection = Net::HTTP.new('localhost', '5984')
    connection.request Net::HTTP::Delete.new('/test1')
    @db = Database.new('test1')
    @db.create!
  end
  
  def test_view_creation
    assert view = @db.view('overview', 'view1')
    source = "function (doc) { map(doc._id, doc); }"
    view.source = source
    assert view.save
    
    overview = @db.retrieve('_design/overview')
    assert_equal overview['views']['view1'], source
    
    view2 = @db.view('overview', 'view1')
    assert_equal view.source, view2.source
    
    assert @db.save([
      Document.new('a' => 1),
      Document.new('b' => 2),
      Document.new('c' => 3)
    ])

    view = @db.view('overview', 'view1')
    
    docs = view.find
    assert_equal 3, docs.length
    docs.each do |d|
      # ugly
      assert_equal 1, d['a'] if d['a']
      assert_equal 2, d['b'] if d['b']
      assert_equal 3, d['c'] if d['c']
    end
  
    docs2 = view.find(:key => docs.first.id)
    assert_equal 1, docs2.length
  end
end