$: << File.expand_path(File.dirname(__FILE__))

require 'rubygems'

begin
  require 'json/ext'
rescue LoadError
  puts "Using pure ruby JSON lib"
  require 'json/pure'
end

module CouchDB
  VERSION = '0.0.1'
  autoload :Overview, 'lib/overview'
  autoload :View, 'lib/view'
  autoload :Document, 'lib/document'
  autoload :Database, 'lib/database'
  autoload :Response, 'lib/response'
end

