module CouchDB
  # A overview is a document containing the source of multiple views
  class Overview < Document
    def self.open_or_create(database, name, language="text/javascript")
      if overview = open(database, name)
        return overview 
      end
      overview = Overview.new(name, database)
      overview.save
      overview
    end
    
    def self.open(database, name)
      super(database, Overview.id(name))
    end
    
    def initialize(name, database, language="text/javascript")
      super({ 
        '_id' => Overview.id(name),
        'language' => language,
        'views' => {}
      }, database)
    end
    
    private
    
    def self.id(name)
      "_design/#{name}"
    end
  end
end