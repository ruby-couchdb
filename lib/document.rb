module CouchDB
  class Document
    attr_reader :attributes, :id, :rev
    alias_method :revision, :rev

    def self.open(db, doc_id)
      res = db.get( db.path(doc_id) )
      return nil if res.kind_of?(Net::HTTPClientError)
      doc = Document.new(JSON.parse(res.body), db)
      doc
    end
    
    def initialize(hash = {}, db = nil)
      update_from_hash(hash, db)
    end
    
    def [](k)
      @attributes[k]
    end
    
    def []=(k,v)
      @attributes[k]=v
    end
    
    def id=(id)
      if @id.nil?
        @id = id
      else
        raise "ID already set for Document (#{@id}). Cannot change."        
      end
    end
    
    def database
      if @database.nil?
        raise "This document hasn't been saved to a database yet."
      else
        @database
      end
    end
    
    def path
      @id.nil? ? nil : database.path(@id)
    end
    
    def reload
      res = database.get(path)
      return nil if res.kind_of?(Net::HTTPClientError)
      update_from_hash JSON.parse(res.body)
    end
    
    def save
      database.save(self)
    end
    
    def to_hash(include_id = true, include_rev = true)
      additional = {}
      additional['_id'] = @id if include_id and @id
      additional['_rev'] = @rev if include_rev and @rev
      @attributes.merge(additional)
    end
    
    def to_json
      to_hash.to_json
    end
    
    def update_from_hash(hash, db=nil)
      @database = db if db
      @attributes = hash
      @id = @attributes.delete('_id')
      @rev = @attributes.delete('_rev')
    end
    
    def update_from_result(result, db=nil)
      @database = db if db
      if result['ok']
        @id = result['id']
        @rev = result['rev']
      end
    end
    
    def inspect
      a = []
      a << "database=#{database.url}" if @database
      a << "id=#{@id.inspect}" if @id
      a << "rev=#{@rev.inspect}" if @rev
      a << "#{@attributes.inspect}"
      "<CouchDB::Document #{a.join(' ')}>"
    end
  end
end