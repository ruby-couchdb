module CouchDB
  # CouchDB::View is not a true CouchDB::Document, but has a similar
  # interface. The most important method is find.
  class View
    
    def initialize(database, overview_name, name, _source=nil)
      @name = name
      @overview_name = overview_name
      @database = database
      source = _source unless _source.nil?
    end
    
    def save
      overview.save
    end
    
    def reload
      overview.reload
    end
    
    def delete
      overview['views'].delete(@name)
      save
    end
    
    def source
      overview['views'][@name]
    end
    
    def source=(s)
      overview['views'][@name] = s
    end
    
    # :key=keyvalue
    # :startkey=keyvalue
    # :startkey_docid=docid
    # :endkey=keyvalue
    # :count=max rows to return
    # :update=false
    # :descending=true
    # :skip=rows to skip
    def find(opts = {})
      res = @database.get(path, opts)
      if res.kind_of?(Net::HTTPClientError)
        raise "Error querying view! #{res.message}" 
      end
      
      res_body = JSON.parse(res.body)
      p res_body
      rows = res_body['rows']
      
      rows.map { |row| Document.new(row['value'], @database) }
    end
    
    def path
      @database.path('_view', @overview_name, @name)
    end
    
    protected
    
    def overview
      @overview ||= Overview.open_or_create(@database, @overview_name)
    end
  
  end
end
