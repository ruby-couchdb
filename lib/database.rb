require "net/http"

module CouchDB
  class Database
    def initialize(name, host = 'localhost', port = '5984')
      @name = name
      @host = host
      @port = port
      @connection = Net::HTTP.new(@host, @port)
      @connection.set_debug_output($stderr) if $debug
    end
    
    def url
      "http://#{@host}:#{@port}/#{@name}"
    end
    
    def path(*c)
      File.join('/', @name, *c)
    end
    
    def exists?
      res = get('/' + @name)
      res.kind_of?(Net::HTTPNotFound) ? false : true
    end
    
    def create!
      res = put('/' + @name)
      raise "Could not create database" if res.kind_of?(Net::HTTPClientError)
    end
    
    def retrieve(doc_id)
      Document.open(self, doc_id)
    end
    
    def view(overview_name, view_name)
      View.new(self, overview_name, view_name)
    end
    
    def save(doc)
      return bulk_save(doc) if(doc.kind_of?(Array))
      
      method = nil
      
      res = if doc.id.nil?
              method = :post
              post(path, doc.to_json)
            else
              method = :put
              put(path(doc.id), doc.to_json)
            end
      #return nil if res.kind_of?(Net::HTTPClientError)
      raise res.message if res.kind_of?(Net::HTTPClientError)
  
      res_body = JSON.parse(res.body)
      
      doc.update_from_result(res_body, self)
      doc.revision
    end
    
    def bulk_save(docs)
      res = post(path('_bulk_docs'), docs.to_json)
      # {
      #   "ok": true,
      #   "results": [
      #     {"ok":true,"id":"D88F7C33AEA8BD3FBACD4F05CF602DC2","rev":"3465402201"},
      #     {"ok":true,"id":"6A139CEFDA35340810A554A9CEA439D1","rev":"914573466"},
      #     {"ok":true,"id":"1097ED566C43011E8ED5F54088CFA1C9","rev":"2541681057"}
      #   ]
      # }
      return false if res.kind_of?(Net::HTTPClientError)
      res_body = JSON.parse(res.body)
      return false unless res_body['ok']
      
      # CouchDB Bug!!! Doc Results are reversed
      # res_body['results'].reverse.zip(docs).each do |result, doc|
      res_body['results'].zip(docs).each do |result, doc|
        doc.update_from_result(result, self)
      end
      
      true
    end
    
    # Send a GET request to +path+
    def get(path, params={})
      path += '?' + self.class.query_string(params) unless params.empty? 
      puts "\nGET path: #{path.inspect}" if $debug
      request(Net::HTTP::Get.new(path))
    end
    
    # Send a POST request to +path+ with the body payload of +data+
    # +content_type+ is the Content-Type header to send along (defaults to
    # application/json)
    def post(path, data=nil, content_type="application/json")
      post = Net::HTTP::Post.new(path)
      post["Content-Type"] = content_type
      post.body = data
      request(post)
    end
    
    # Send a PUT request to +path+ with the body payload of +data+
    # +content_type+ is the Content-Type header to send along (defaults to
    # application/json)
    def put(path, data=nil, content_type="application/json")
      put = Net::HTTP::Put.new(path)
      put["Content-Type"] = content_type
      put.body = data
      request(put)
    end
    
    # Send a DELETE request to +path+
    def delete(path)
      req = Net::HTTP::Delete.new(path)
      request(req)
    end
    
    protected
    
    # send off a +req+ object to the host. req is a Net::Http:: request class
    # (eg Net::Http::Get/Net::Http::Post etc)
    def request(req)
      r  = @connection.request(req)
    end
    
    module CharacterClasses
      ALPHA = "a-zA-Z"
      DIGIT = "0-9"
      GEN_DELIMS = "\\:\\/\\?\\#\\[\\]\\@"
      SUB_DELIMS = "\\!\\$\\&\\'\\(\\)\\*\\+\\,\\;\\="
      RESERVED = GEN_DELIMS + SUB_DELIMS
      UNRESERVED = ALPHA + DIGIT + "\\-\\.\\_\\~"
      PCHAR = UNRESERVED + SUB_DELIMS + "\\:\\@"
      SCHEME = ALPHA + DIGIT + "\\-\\+\\."
      AUTHORITY = PCHAR
      PATH = PCHAR + "\\/"
      QUERY = PCHAR + "\\/\\?"
      FRAGMENT = PCHAR + "\\/\\?"
    end

    def self.encode_segment(segment, character_class=CharacterClasses::RESERVED +
        CharacterClasses::UNRESERVED)
      return nil if segment.nil?
      return segment.gsub(
        /[^#{character_class}]/
      ) do |sequence|
        ("%" + sequence.unpack('C')[0].to_s(16).upcase)
      end      
    end

    def self.query_string(query)
      if query.respond_to?(:each_pair) # Some sort of Hash
        query_strings = []
        query.each_pair do |key, value|
          query_strings << (value.nil? ? [key] : [key, value]).map do |e| 
            encode_segment(e.to_s, CharacterClasses::UNRESERVED)
          end.join('=')
        end
        return query_strings.join('&')

      elsif query.respond_to?(:each) # Some sort of Array
        query.each do |key|
          encode_segment(key.to_s, CharacterClasses::UNRESERVED)
        end.join('&')

        return query 
      end
    end
  end
end